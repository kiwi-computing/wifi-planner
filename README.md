**In Beta state!!**

# Wifi planner

A program that gives an estimate of where to place your wifi router.

Read more about the program here: [kiwi-computing.dk](https://kiwi-computing.dk/announcement/wifi-planner/)

# Requirements
* numpy
* PIL
* CV2

# Features
*  Finds the most optimale placement of a wifi router with an user inputted strength
*  Analyze a house plan to finde the different rooms and the barries of the house

# Up comming features
*  Check for both 2,4 GHz and 5 GHz
*  Optimize for best coverage in procent, when using more than 1 router 

# Contributors
[Kiwimarc](https://gitlab.com/kiwimarc)  
[Ninjajen](https://gitlab.com/Ninjajen)
