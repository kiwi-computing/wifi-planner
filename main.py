import cv2
import numpy as np
import math

def main():
    a = input("Enter image file name: ")
    Image(a)

class Image():
    def __init__(self, a):
        self.load_img(a)
        
    def load_img(self, a):
        #Read image in gray scale
        self.raw = cv2.imread("input/{}".format(a),0)
        self.image = cv2.cvtColor(self.raw.copy(),cv2.COLOR_GRAY2RGB);
        self.img = cv2.blur(self.raw.copy(),(5,5))
        a = input("Are the image clean from doors, furnitures and other kind of noise? (y/n) ")
        if (a == "y"):
            Rooms(self.img,self.image)
        elif (a == "n"):
            print("Removing noise")
            self.noise_remove(self.img)
            Rooms(self.img,self.image)
            
    def noise_remove(self, img):
        self.mask = cv2.threshold(self.img,127,255,cv2.THRESH_BINARY)[1]
        self.dst = cv2.inpaint(self.img, self.mask, 7, cv2.INPAINT_NS)
        self.img = self.dst
        return self.img

    def display_img(image):
       cv2.imshow("Result",image)
       cv2.waitKey()
       cv2.destroyAllWindows()
    
    def save_img(name,img):
        # Image path 
        filename = r"export/{}".format(name)
        # Saving the image 
        cv2.imwrite(filename, img) 

class Rooms():
    def __init__(self, img, image):
        rooms, colored_house, Dist, w, h = self.find_rooms(img)
        x_image, y_image = Dist[0][0]
        ## Make one pixel red;
        image[int(x_image),int(y_image)] = [0,0,255]
        image = cv2.circle(image, (y_image,x_image), 20, (255, 0, 0), 4) 
            
        Image.save_img("ColorHouseOverlay.jpg",colored_house)
        power = float(input("What is transmitpower of your router? (dBm) "))
        area = float(input("What is the area of your house? (m^2) "))
        image = Placement.rangerouter(power, area, h, w, y_image, x_image, image)
        Image.save_img("Result.jpg",image)
        Image.display_img(image)
        
    def find_rooms(self, img, corners_threshold=0.1, room_closing_max_length=100, gap_in_wall_threshold=700):
        img[img < 50] = 0
        img[img > 60] = 255
        Image.save_img("NoiseCleanHouse.jpg",img)
        # Detect corners (you can play with the parameters here)
        dst = cv2.cornerHarris(img ,2,3,0.04)
        dst = cv2.dilate(dst,None)
        corners = dst > corners_threshold * dst.max()
        
        # Draw lines to close the rooms off by adding a line between corners on the same x or y coordinate
        # This can gets some false positives.
        walls_x = []
        for y,row in enumerate(corners):
            x_same_y = np.argwhere(row)
            for x1, x2 in zip(x_same_y[:-1], x_same_y[1:]):
                if x2[0] - x1[0] < room_closing_max_length:
                    color = 0
                    cv2.line(img, (x1, y), (x2, y), color, 1)
                    walls_x.append((int(x1),int(x2)))
        # width and height divided by 4 to make space between the dots.                    
        walls_y = []
        for x,col in enumerate(corners.T):
            y_same_x = np.argwhere(col)
            for y1, y2 in zip(y_same_x[:-1], y_same_x[1:]):
                if y2[0] - y1[0] < room_closing_max_length:
                    color = 0
                    cv2.line(img, (x, y1), (x, y2), color, 1)
                    walls_y.append((int(y1),int(y2)))

        # Mark the outside of the house as black
        contours, _ = cv2.findContours(~img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]
        biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
        mask = np.zeros_like(img)
        house = cv2.fillPoly(mask, [biggest_contour], 255)
        # Get bounding box
        # For some weird reason is height and width switched
        self.x, self.y, h, w = cv2.boundingRect(house)
        img[mask == 0] = 0

        # Find the connected components in the house
        ret, labels = cv2.connectedComponents(img)
        img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
        
        unique = np.unique(labels)
        rooms = []
        for label in unique:
            component = labels == label
            if img[component].sum() == 0 or np.count_nonzero(component) < gap_in_wall_threshold:
                color = 0
            else:
                rooms.append(component)
                color = np.random.randint(0, 255, size=3)
            img[component] = color
        Image.save_img("ColorHouse.jpg",img)    
        # Make one pixel red
        # Some adjustments was needed to make it fit correctly
        # width and height divided by 4 to make space between the dots.
        coords = []
        for i in range(int(w/4-5)):
            self.x1 = self.x+4*i-4
            for j in range(int(h/4-5)):
                self.y1 = self.y+4*j+26
                img[self.x1,self.y1]=[255,0,0]
                coords.append((self.x1,self.y1))
        b = Placement.bound(self.x,self.y,w,h)
        Dist = Placement.dis(coords, b, walls_x, walls_y)
        Dist = Placement.throughwall(Dist, walls_x, walls_y)
        
        return rooms, img, Dist, w, h

class Placement():       

    def bound(x, y, w, h):
        print("Adding the boundares")
        b = [(x+i,y) for i in range(w-x+1)]
        b.extend([(x,y+i) for i in range(h-y+1)])
        return b
    
    def dis(coords, b, walls_x, walls_y):
        print("Finding best postion in house")
        Dist = [(i,sum([((i[0]-j[0])**2+(i[1]-j[1])**2)**(1/2) for j in b])) for i in coords]
        Dist.sort(key=lambda x: x[1])
        return Dist
        
    def throughwall(Dist, walls_x, walls_y):
        point = []
        for i in Dist:
            for x,y in zip(walls_x,walls_y):
                if (i[0][0] > x[0] and i[0][0] < x[1] and i[0][1] > y[0] and i[0][1] < y[1]):
                    point.append((i[0],i[1]**5))
                else:
                    point.append(i)
        point.sort(key=lambda x: x[1])
        return point
    
    def rangerouter(power, area, h, w, y_image, x_image, image):
        print("Checking range")
        m = float(power-(-40)-30)
        r = (10**(((m-32.44-20*math.log10(2450))/20)))/0.0010000
        pa = (h*w)/area
        rp = r * pa
        print("Finishing up")
        overlay = image.copy()
        cv2.circle(overlay, (y_image,x_image), int(rp), (0, 0, 255), -1)
        image = cv2.addWeighted(overlay, 0.4, image, 1-0.4, 0);
        return image

if __name__ == '__main__':
    main()